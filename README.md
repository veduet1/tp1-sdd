# TP1 SDD

## Structure

Le projet est structuré ainsi:
```
build/              – Données de construction du programme
tests/              – Fichiers de tests du programme
src/                – Code source du programme
    helper.c        – Implémentation de fonctions génériques utiles au problème
    liste_action.c  – Implémentation de la liste chaînée des actions
    liste_semaine.c – Implémentation de la liste chaînée des semaines
    liste_noms.c    – Implémentation de la liste contigue des noms
    helper.h        – En–tête des fonctions génériques utiles au problème
    liste_action.h  – En–tête de la liste chaînée des actions
    liste_semaine.h – En–tête de la liste chaînée des semaines
    liste_noms.h    – En–tête de la liste contigue des noms
MakeFile            – Fichier à appeler pour la compilation du programme
README.md           – Documentation de base du programme
```