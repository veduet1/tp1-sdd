/* -------------------------------------------------------------------- */
/* liste_action.h                                                       */
/*                                                                      */
/* Liste chainée contenant les actions dans un agenda                   */
/* /!\ Les actions ne sont pas forcément triés par ordre chronologique  */
/* -------------------------------------------------------------------- */

#ifndef LISTE_ACTION_H_INCLUDED
#define LISTE_ACTION_H_INCLUDED

/* -------------------------------------------------------------------- */
/* bloc_action     Structure d’un bloc de la liste chainée              */
/*                                                                      */
/* Champs: jour    – Jour de la semaine codé sur un caractère ASCII et  */
/*                   prenant une valeur comprise entre '0' et '7'       */
/*         heure   – L'heure de la semaine codé sur deux caractères     */
/*                   ASCII et prenant une valeur comprise entre '00' et */
/*                   '24'                                               */
/*                   /!\ Les caractères non numériques sont interdits   */
/*         nom     – Le nom de l’action codé sur une chaine de 10       */
/*                   caractères                                         */
/*         suivant – Pointeur vers le bloc suivant de semaine           */
/* -------------------------------------------------------------------- */
struct bloc_action {
    char                jour;
    char                heure[2];
    char                nom[11];
    struct bloc_action *suivant;
};

/* -------------------------------------------------------------------- */
/* liste_action    Point d’entrée vers la liste chaînée des semaines    */
/*                                                                      */
/* Champs: premier – Pointeur vers le premier bloc d’action             */
/* -------------------------------------------------------------------- */
struct liste_action {
    struct bloc_action *premier;
};

/* -------------------------------------------------------------------- */
/* bloc_action_t       Redéfinition de struct bloc_action               */
/* -------------------------------------------------------------------- */
typedef struct bloc_action bloc_action_t;

/* -------------------------------------------------------------------- */
/* liste_action_t      Redéfinition de struct liste_action              */
/* -------------------------------------------------------------------- */
typedef struct liste_action liste_action_t;

/* -------------------------------------------------------------------- */
/* liste_action_creer       Initialise liste_action_t                   */
/*                                                                      */
/* En sortie: Une liste vide d’actions                                  */
/* -------------------------------------------------------------------- */
liste_action_t liste_action_creer(void);

/* -------------------------------------------------------------------- */
/* liste_action_liberer      Libère liste_action_t                      */
/*                                                                      */
/* En entrée: liste – la liste d’actions à libérer                      */
/* -------------------------------------------------------------------- */
void liste_action_liberer(liste_action_t *liste);

/* -------------------------------------------------------------------- */
/* liste_action_inserer      Insère un bloc à la liste d’actions        */
/*                                                                      */
/* En entrée: liste – Liste des actions où insérer l’action             */
/*            jour  – Jour de la semaine où l’action se déroule         */
/*            heure – Heure où l’action se déroule sur 2 caractères     */
/*            nom   – Nom de l’action sur 10 caractères                 */
/*                                                                      */
/* En sortie: 1 si l’élément est inséré, 0 sinon                        */
/* -------------------------------------------------------------------- */
int liste_action_inserer(liste_action_t *liste,
                         char            jour,
                         char           *heure,
                         char           *nom);

/* -------------------------------------------------------------------- */
/* liste_action_supprimer                                               */
/*           Supprime le premier bloc de la liste de actions            */
/*                                                                      */
/* En entrée: liste – Liste des actions où retirer l’action             */
/*                                                                      */
/* En sortie: L’action retiré ou NULL si la liste est vide              */
/* -------------------------------------------------------------------- */
bloc_action_t *liste_action_supprimer(liste_action_t *liste);

/* -------------------------------------------------------------------- */
/* liste_action_supprimer_par_donnees                                   */
/*                Recherche et supprime l’action désirée                */
/*                                                                      */
/* En entrée: liste – Liste de semaines où rechercher la semaine        */
/*            jour  – Jour de l’action à supprimer                      */
/*            heure – Heure de l’action à supprimer                     */
/*                                                                      */
/* En sortie: 1 si la suppression s’est bien effectuée et l’action      */
/*            trouvée ou 0 sinon                                        */
/* -------------------------------------------------------------------- */
int liste_action_supprimer_par_donnees(liste_action_t *liste,
                                       char            jour,
                                       char           *heure);
                                       
/* -------------------------------------------------------------------- */
/* liste_action_recherche   Recherche l’adresse de l’action souhaitée   */
/*                                                                      */
/* En entrée: liste – Liste des actions où rechercher l’action          */
/*            jour  – Jour de l’action recherchée                       */
/*            heure – Heure de l’action recherchée                      */
/*                                                                      */
/* En sortie: L’adresse de la case représentant l’action recherchée si  */
/*            trouvée ou NULL sinon                                     */
/* -------------------------------------------------------------------- */
bloc_action_t *liste_action_recherche(liste_action_t liste,
                                      char           jour,
                                      char          *heure);

#endif
