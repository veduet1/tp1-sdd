/* -------------------------------------------------------------------- */
/* helper.h                                                             */
/*                                                                      */
/* Collection des fonctions utiles au problème                          */
/* -------------------------------------------------------------------- */

#ifndef HELPER_H_INCLUDED
#define HELPER_H_INCLUDED

#include <stdlib.h>

/* -------------------------------------------------------------------- */
/* cmp_tableau       Teste l’égalité de deux tableaux                   */
/*                                                                      */
/* En entrée: t1, t2 – Tableaux de valeurs codés sur 8 bits à comparer  */
/*            N      – la taille des deux tableaux                      */
/*                                                                      */
/* En sortie: 1 si les deux tableaux contiennent les mêmes N caractères */
/*            et 0 sinon                                                */
/* -------------------------------------------------------------------- */
int cmp_tableau(char *t1, char *t2, size_t N);

/* -------------------------------------------------------------------- */
/* compris_entre                                                        */
/*        Vérifie si le caractère est compris entre deux bornes         */
/*                                                                      */
/* En entrée: v   – Valeur à vérifier codé sur 8 bits                   */
/*            min – Borne minimale codé sur 8 bits                      */
/*            max – Borne maximale codé sur 8 bits                      */
/*                                                                      */
/* En sortie: 1 si v est compris entre min et max et 0 sinon            */
/* -------------------------------------------------------------------- */
int compris_entre(char v, char min, char max);

/* -------------------------------------------------------------------- */
/* cpy_desc     Copie d’une chaine de caractères sans saut de ligne     */
/*                                                                      */
/* En entrée: dest – Tableau à destination des données                  */
/*            orig – Tableau d’origine des données                      */
/*            N    – Taille des deux tableaux                           */
/* -------------------------------------------------------------------- */
void cpy_desc(char *dest, char *orig, size_t N);

/* -------------------------------------------------------------------- */
/* cpy_liste_chiffre           Copie d’un tableau de chiffres           */
/*                                                                      */
/* En entrée: dest – Tableau à destination des données                  */
/*            orig – Tableau d’origine des données                      */
/*            min  – Borne minimale de la valeur numérique              */
/*            max  – Borne maximale de la valeur numérique              */
/*            N    – Taille des deux tableaux                           */
/*                                                                      */
/* En sortie: 1 si l’origine ne contient que des chiffres compris       */
/*            numériquement entre min et max ou 0 sinon.                */
/* -------------------------------------------------------------------- */
int cpy_liste_chiffre(char *dest,
                      char *orig,
                      char *min,
                      char *max,
                      size_t N);

/* -------------------------------------------------------------------- */
/* taille_chaine_limite                                                 */
/*    Calcule la taille de la chaine de caractère dans un tableau de    */
/*                               taille N                               */
/*                                                                      */
/* En entrée: dat – Chaîne de caractères à mesurer                      */
/*            N   – Taille maximale du tableau de données               */
/*                                                                      */
/* En sortie: La valeur minimale entre la taille de dat et N.           */
/* -------------------------------------------------------------------- */
size_t taille_chaine_limite(char *dat, size_t N);

/* -------------------------------------------------------------------- */
/* verifie_motif                                                        */
/*     Vérifie que une chaine de caractères possède un motif donné      */
/*                                                                      */
/* En entrée: val   – Tableau où chercher le motif                      */
/*            motif – Chaine de caractères composant le motif           */
/*            N     – Taille maximale du tableau val                    */
/*                                                                      */
/* En sortie: 1 si un motif a été détecté dans val ou 0 sinon.          */
/* -------------------------------------------------------------------- */
int verifie_motif(char *val, char *motif, size_t N);

#endif
