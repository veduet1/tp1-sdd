/* -------------------------------------------------------------------- */
/* liste_semaine.c                                                      */
/*                                                                      */
/* Gère la liste chainée codant les semaines dans l'agenda              */ 
/* -------------------------------------------------------------------- */

#include "liste_semaine.h"
#include "helper.h"

#include <stdlib.h>
#include <string.h>

/* -------------------------------------------------------------------- */
/*                                                                      */
/* En-tête des procédures privées                                       */
/*                                                                      */
/* -------------------------------------------------------------------- */

/* -------------------------------------------------------------------- */
/* resultat_recherche_bloc_semaine                                      */
/*    Structure d’un résultat de recherche d’une semaine de la liste    */
/*                                                                      */
/* Champs: element   – Pointeur vers l’élément recherché si trouvé ou   */
/*                     NULL sinon                                       */
/*         precedent – Pointeur vers l’élément précédent l’élément      */
/*                     recherché si ce dernier et trouvé et ne se       */
/*                     trouve pas en première position ou NULL sinon    */
/* -------------------------------------------------------------------- */
struct resultat_recherche_bloc_semaine {
    struct bloc_semaine* element;
    struct bloc_semaine* precedent;
};

/* -------------------------------------------------------------------- */
/* liste_semaine_recherche       Récupère l’adresse de la semaine       */
/*                                                                      */
/* En entrée: liste   – Liste des semaines où rechercher la semaine     */
/*            annee   – Année de la semaine recherchée                  */
/*            semaine – Semaine recherchée                              */
/*                                                                      */
/* En sortie: Un objet contenant les adresse de la semaine recherchée   */
/*            dans le champ element et celle de la semaine la précédent */
/*            dans la liste (eg celle dont le champ suivant pointe sur  */
/*            la même case mémoire que element) dans precedent          */
/* -------------------------------------------------------------------- */
static
struct resultat_recherche_bloc_semaine
liste_semaine_recherche_intermediaire(
    liste_semaine_t liste,
    char           *annee,
    char           *semaine)
{
    struct resultat_recherche_bloc_semaine ret   = { NULL, NULL };
    bloc_semaine_t                        *cour1 = liste.premier;
    bloc_semaine_t                        *cour2 = NULL;

    while (ret.element == NULL && cour1 != NULL) {
        if (cmp_tableau(cour1->annee, annee, 4) &&
            cmp_tableau(cour1->semaine, semaine, 2))
        {
            ret.element   = cour1;
            ret.precedent = cour2;
        }

        cour2 = cour1;
        cour1 = cour1->suivant;
    }

    return ret;
}

/* -------------------------------------------------------------------- */
/*                                                                      */
/* Implémentation des fonctions                                         */
/*                                                                      */
/* -------------------------------------------------------------------- */

liste_semaine_t liste_semaine_creer(void)
{
    return (liste_semaine_t){ NULL };
}

void liste_semaine_liberer(liste_semaine_t *liste) {
    bloc_semaine_t *cour = NULL;
    bloc_semaine_t *suiv = NULL;

    if (liste != NULL)
    {
        cour = liste->premier;
        liste->premier = NULL;

        while (cour != NULL)
        {
            suiv = cour->suivant;

            bloc_semaine_liberer(&cour);

            cour = suiv;
        }
    }
}

void bloc_semaine_liberer(bloc_semaine_t **bloc)
{
    if (bloc != NULL && (*bloc) != NULL) {
        liste_action_liberer(&(*bloc)->actions);
        free(*bloc);
        *bloc = NULL;
    }
}

int liste_semaine_inserer(liste_semaine_t *liste,
                          char            *annee,
                          char            *semaine,
                          liste_action_t   actions)
{
    bloc_semaine_t *a_inserer = NULL;
    int             ok        = 1;

    if (liste != NULL && annee != NULL && semaine != NULL) {
        a_inserer = malloc(sizeof(bloc_semaine_t));

        if (a_inserer != NULL)
        {
            if (!cpy_liste_chiffre(
                    a_inserer->annee, annee, "0000", "9999", 4) ||
                !cpy_liste_chiffre(
                    a_inserer->semaine, semaine, "01", "53", 2))
            {
                ok = 0;
                free(a_inserer);
            }
            else
            {
                a_inserer->actions = actions;
                
                a_inserer->suivant = liste->premier;
                liste->premier = a_inserer;
            }
        }
    }

    return ok;
}

int liste_semaine_inserer_evenement(liste_semaine_t *liste,
                                    char            *annee,
                                    char            *semaine,
                                    char             jour,
                                    char            *heure,
                                    char            *description)
{
    bloc_semaine_t *bloc_semaine = NULL;
    liste_action_t  actions;
    int             ok           = 1;

    if (liste != NULL)
    {
        bloc_semaine = liste_semaine_recherche(*liste, annee, semaine);

        if (bloc_semaine == NULL) {
            actions = liste_action_creer();

            ok = liste_action_inserer(&actions, jour, heure, description);

            if (ok)
            {
                ok = liste_semaine_inserer(
                    liste, annee, semaine, actions);

                if (!ok)
                {
                    liste_action_liberer(&actions);
                }
            }
        } else {
            if (liste_action_recherche(bloc_semaine->actions, jour, heure))
            {
                ok = 0;
            }
            else
            {
                ok = liste_action_inserer(
                    &bloc_semaine->actions, jour, heure, description);
            }
        }
    }

    return ok;
}

bloc_semaine_t *liste_semaine_recherche(liste_semaine_t liste,
                                        char           *annee,
                                        char           *semaine)
{
    return liste_semaine_recherche_intermediaire(liste, annee, semaine)
        .element;
}

int liste_semaine_supprime(liste_semaine_t *liste)
{
    bloc_semaine_t *case_retire = NULL;
    int ret = 0;

    if (liste != NULL)
    {
        case_retire = liste->premier;
        liste->premier = case_retire->suivant;
        case_retire->suivant = NULL;

        bloc_semaine_liberer(&case_retire);

        ret = 1;
    }

    return ret;
}

int liste_semaine_supprimer_action(liste_semaine_t *liste,
                                   char            *annee,
                                   char            *semaine,
                                   char             jour,
                                   char            *heure)
{
    struct resultat_recherche_bloc_semaine sem;
    liste_action_t                         actions;
    int                                    ok      = 0;

    sem = liste_semaine_recherche_intermediaire(*liste, annee, semaine);

    if (sem.element != NULL)
    {
        actions = sem.element->actions;
        ok = liste_action_supprimer_par_donnees(&actions, jour, heure);

        sem.element->actions = actions;

        if (actions.premier == NULL)
        {
            if (sem.precedent != NULL)                                      /* Modification de la liste afin d’isoler l’élément à supprimer */
            {
                sem.precedent->suivant = sem.element->suivant;
            }
            else
            {
                liste->premier = sem.element->suivant;
            }

            free(sem.element);
        }
    }

    return ok;    
}

int liste_semaine_charge_depuis_fichier(FILE *f,
                                        liste_semaine_t *recepteur)
{
    int    retour     = 0;
    size_t taille_buf = 0;
    char   buf[20]    = { 0 };
    char   c          = 0;

    char *annee    = buf;
    char *semaine  = buf + 4;
    char *jour     = buf + 6;
    char *heure    = buf + 7;
    char *intitule = buf + 9;

    if (f != NULL && recepteur != NULL)
    {
        retour = 1;

        while (retour == 1 && fgets(buf, 20, f) != NULL) {
            taille_buf = taille_chaine_limite(buf, 20);

            if (taille_buf == 19 && buf[19] != '\n')                        /* On élimine les caractères en trop dans la ligne */
            {
                do
                {
                    c = fgetc(f);
                } while (c != '\n' && c != EOF);
            }

            retour = liste_semaine_inserer_evenement(recepteur,
                                                     annee,
                                                     semaine,
                                                     *jour,
                                                     heure,
                                                     intitule);
        }
    }

    return retour;
}

int liste_semaine_enregistre_vers_fichier(FILE *f,
                                          liste_semaine_t donneur)
{
    int             retour      = 1;
    bloc_semaine_t *cur_semaine = NULL;
    bloc_action_t  *cur_action  = NULL;

    if (f != NULL)
    {
        cur_semaine = donneur.premier;

        while (cur_semaine != NULL && retour)
        {
            cur_action = cur_semaine->actions.premier;

            while (cur_action != NULL && retour)
            {
                retour = fprintf(f, "%.*s%.*s%c%.*s%-10.*s\n",              /* En cas d’erreur, la fonction fprintf renvoie une valeur négative */
                    4, cur_semaine->annee,
                    2, cur_semaine->semaine,
                    cur_action->jour,
                    2, cur_action->heure,
                    10, cur_action->nom
                ) > 0;

                cur_action = cur_action->suivant;
            }

            cur_semaine = cur_semaine->suivant;
        }
    }

    return retour;
}

void liste_semaine_afficher(liste_semaine_t liste)
{
    bloc_semaine_t *semaine = liste.premier;
    bloc_action_t  *action  = NULL;

    while (semaine != NULL) {
        action = semaine->actions.premier;

        printf("Semaine %.*s de l’an %.*s:\n", 2, semaine->semaine, 4, semaine->annee);

        while (action != NULL)
        {
            printf("\tJour %c à %.*sh: %.*s\n", action->jour, 2, action->heure, 10, action->nom);

            action = action->suivant;
        }
        
        semaine = semaine->suivant;
    }
}