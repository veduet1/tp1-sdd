/* -------------------------------------------------------------------- */
/* helper.c                                                             */
/*                                                                      */
/* Implémentation des fonctions utiles au problème                      */
/* -------------------------------------------------------------------- */

#include "helper.h"

int cmp_tableau(char *t1, char *t2, size_t N)
{
    size_t i  = 0;
    int    ok = 0;

    if (t1 != NULL && t2 != NULL)
    {
        ok = 1;

        while (ok && i < N)
        {
            if (t1[i] != t2[i])
            {
                ok = 0;
            }

            i ++;
        }
    }

    return ok;
}

int compris_entre(char v, char min, char max)
{
    return v >= min || v <= max;
}

void cpy_desc(char *dest, char *orig, size_t N)
{
    size_t i  = 0;
    int    ok = 0;

    if (dest != NULL && orig != NULL)
    {
        ok = 1;

        while (ok && i < N)
        {
            if (orig[i] == '\n')
            {
                ok = 0;
            }
            else
            {
                dest[i] = orig[i];
            }

            i++;
        }
    }
}

int cpy_liste_chiffre(char *dest,
                      char *orig,
                      char *min,
                      char *max,
                      size_t N)
{
    size_t i     = 0;
    int    ok    = 1;
    int    check = 0;

    if (dest != NULL && orig != NULL && min != NULL && max != NULL)
    {
        while (ok && i < N)
        {
            if (!compris_entre(orig[i], '0', '9'))
            {
                ok = 0;
            }
            else
            {
                if (i == 0 || check < 0)                                    /* Vérification de la valeur minimale */
                {
                    if (orig[i] < min[i])
                    {
                        ok = 0;
                    }    
                    else if (orig[i] == min[i])
                    {
                        check = -1;
                    }
                    else
                    {
                        check = 0;
                    }
                }
                
                if ((i == 0 && check == 0) || check > 0)                    /* Vérification de la valeur maximale */
                {
                    if (orig[i] > max[i])
                    {
                        ok = 0;
                    }
                    else if (orig[i] == max[i])
                    {
                        check = 1;
                    }
                    else
                    {
                        check = 0;
                    }
                }
            }

            if (ok)
            {
                dest[i] = orig[i];

                i++;
            }
        }
    }

    return ok;
}

size_t taille_chaine_limite(char *dat, size_t N)
{
    size_t taille = 0;

    if (dat != NULL)
    {
        while (taille < N && dat[taille] != '\0')
        {
            taille ++;
        }
    }

    return taille;
}

int verifie_motif(char *val, char *motif, size_t N)
{
    size_t position_motif;
    size_t position_val   = 0;
    size_t ret            = 0;

    while (position_val < N && !ret)
    {
        position_motif = 0;

        if (val[position_val] == motif[position_motif])                     /* Le premier caractère du motif a été détecté */
        {
            ret = 1;

            while (motif[position_motif] != '\0' && ret)
            {
                ret = motif[position_motif] == val[position_val];

                position_motif ++;
            }
        }
        
        position_val ++;
    }

    if (motif[position_motif] == '\0')
    {
        ret = 1;
    }

    return ret;
}
