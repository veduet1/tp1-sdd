/* -------------------------------------------------------------------- */
/* liste_semaine.h                                                      */
/*                                                                      */
/* Liste chainée contenant les semaines dans un agenda                  */
/* /!\ Les semaines ne sont pas forcément triés par ordre chronologique */
/* -------------------------------------------------------------------- */

#ifndef LISTE_SEMAINE_H_INCLUDED
#define LISTE_SEMAINE_H_INCLUDED

#include "liste_action.h"

#include <stdio.h>

/* -------------------------------------------------------------------- */
/* bloc_semaine         Structure d’un bloc de la liste chainée         */
/*                                                                      */
/* Champs: annee   – Chaine de caractères codant une année grégorienne  */
/*         semaine – Chaine de caractères codant une semaine sur deux   */
/*                   chiffres (01 pour javier; 12 pour décembre)        */
/*         actions – Liste chaînée codant les actions dans la semaine   */
/*         suivant – Pointeur vers le bloc suivant de semaine           */
/* -------------------------------------------------------------------- */
struct bloc_semaine {
    char                 annee[4];
    char                 semaine[2];
    liste_action_t       actions;
    struct bloc_semaine *suivant;
};

/* -------------------------------------------------------------------- */
/* liste_semaine   Point d’entrée vers la liste chaînée des semaines    */
/*                                                                      */
/* Champs: premier – Pointeur vers le premier bloc de semaine           */
/* -------------------------------------------------------------------- */
struct liste_semaine {
    struct bloc_semaine *premier;
};

/* -------------------------------------------------------------------- */
/* bloc_semaine_t          Redéfinition de struct bloc_semaine          */
/* -------------------------------------------------------------------- */
typedef struct bloc_semaine bloc_semaine_t;

/* -------------------------------------------------------------------- */
/* liste_semaine_t         Redéfinition de struct liste_semaine         */
/* -------------------------------------------------------------------- */
typedef struct liste_semaine liste_semaine_t;

/* -------------------------------------------------------------------- */
/* liste_semaine_creer            Initialise liste_semaine_t            */
/*                                                                      */
/* En sortie: Liste vierge de semaine                                   */
/* -------------------------------------------------------------------- */
liste_semaine_t liste_semaine_creer(void);

/* -------------------------------------------------------------------- */
/* liste_semaine_liberer             Libère liste_semaine_t             */
/*                                                                      */
/* En entrée: liste – Liste de semaines à libérer                       */
/* -------------------------------------------------------------------- */
void liste_semaine_liberer(liste_semaine_t *liste);

/* -------------------------------------------------------------------- */
/* bloc_semaine_liberer             Libère bloc_semaine_t               */
/*                                                                      */
/* En entrée: bloc – Pointeur vers le pointeur du bloc à libérer, ce    */
/*                   dernier ainsi que la liste des événements seront   */
/*                   libérés                                            */
/* -------------------------------------------------------------------- */
void bloc_semaine_liberer(bloc_semaine_t **bloc);

/* -------------------------------------------------------------------- */
/* liste_semaine_inserer      Insère un bloc à la liste de semaines     */
/*                                                                      */
/* En entrée: liste   – Liste des semaines où insérer l’élément         */
/*            annee   – Année de la semaine sur 4 octets                */
/*            semaine – Numéro de semaine sur 2 octets                  */
/*            actions – Liste des actions                               */
/*                                                                      */
/* En sortie: 1 si l’élément est inséré, 0 sinon                        */
/* -------------------------------------------------------------------- */
int liste_semaine_inserer(liste_semaine_t *liste,
                          char            *annee,
                          char            *semaine,
                          liste_action_t   actions);

/* -------------------------------------------------------------------- */
/* liste_semaine_inserer_evenement   Insère un événement dans la liste  */
/*                                                                      */
/* En entrée: liste       – Liste des semaines où insérer l’élément     */
/*            annee       – Année de la semaine sur 4 octets            */
/*            semaine     – Numéro de la semaine sur 2 octets           */
/*            jour        – Jour de la semaine sur 1 octet              */
/*            heure       – Heure de l’événement sur 2 octets           */
/*            description – Description de l’événement sur 10 octets    */
/*                                                                      */
/* En sortie: 1 si l’élément est inséré, 0 sinon                        */
/* -------------------------------------------------------------------- */
int liste_semaine_inserer_evenement(liste_semaine_t *liste,
                                    char            *annee,
                                    char            *semaine,
                                    char             jour,
                                    char            *heure,
                                    char            *description);

/* -------------------------------------------------------------------- */
/* liste_semaine_supprime    Supprime le premier élément de la liste    */
/*                                                                      */
/* En entrée: liste – Liste des semaines où retirer l’élément           */
/*                                                                      */
/* En sortie: 1 si l’élément à supprimer l’est ou 0 si la liste est     */
/*            vide                                                      */
/* -------------------------------------------------------------------- */
int liste_semaine_supprime(liste_semaine_t *liste);

/* -------------------------------------------------------------------- */
/* liste_semaine_recherche  Recherche l’adresse de la semaine souhaitée */
/*                                                                      */
/* En entrée: liste   – Liste de semaines où rechercher la semaine      */
/*            annee   – Année de la semaine recherchée                  */
/*            semaine – Position dans l’année de la semaine recherchée  */
/*                                                                      */
/* En sortie: L’adresse de la case représentant la semaine recherchée   */
/*            si trouvée ou NULL sinon                                  */
/* -------------------------------------------------------------------- */
bloc_semaine_t *liste_semaine_recherche(liste_semaine_t liste,
                                        char           *annee,
                                        char           *semaine);

/* -------------------------------------------------------------------- */
/* liste_semaine_supprimer_action                                       */
/*                Recherche et supprime l’action désirée                */
/*                                                                      */
/* En entrée: liste   – Liste de semaines où rechercher la semaine      */
/*            annee   – Année de l’action à supprimer                   */
/*            semaine – Semaine de l’action à supprimer                 */
/*            jour    – Jour de l’action à supprimer                    */
/*            heure   – Heure de l’action à supprimer                   */
/*                                                                      */
/* En sortie: 1 si la suppression s’est bien effectuée et l’action      */
/*            trouvée ou 0 sinon                                       */
/* -------------------------------------------------------------------- */
int liste_semaine_supprimer_action(liste_semaine_t *liste,
                                   char            *annee,
                                   char            *semaine,
                                   char             jour,
                                   char            *heure);

/* -------------------------------------------------------------------- */
/* liste_semaine_charge_depuis_fichier                                  */
/*    Génère une nouvelle liste des semaines depuis les données d’un    */
/*                               fichier                                */
/*                                                                      */
/* En entrée: f         – Flux à lire                                   */
/*            recepteur – Référence vers la liste réceptrice            */
/*                                                                      */
/* En sortie: 0 si la lecture échou sinon 1                             */
/* -------------------------------------------------------------------- */
int liste_semaine_charge_depuis_fichier(FILE *f,
                                        liste_semaine_t *recepteur);

/* -------------------------------------------------------------------- */
/* liste_semaine_charge_depuis_fichier                                  */
/*   Génère un fichier contenant les données de la liste des semaines   */
/*                                                                      */
/* En entrée: f       – Flux à écrire                                   */
/*            donneur – Structure de données à écrire                   */
/*                                                                      */
/* En sortie: 0 si l’écriture échoue sinon 1                            */
/* -------------------------------------------------------------------- */
int liste_semaine_enregistre_vers_fichier(FILE *f,
                                          liste_semaine_t donneur);

/* -------------------------------------------------------------------- */
/* liste_semaine_afficher   Affiche la liste des semaines dans stdout   */
/*                                                                      */
/* En entrée: liste – Liste des données à afficher                      */
/* -------------------------------------------------------------------- */
void liste_semaine_afficher(liste_semaine_t liste);

#endif
