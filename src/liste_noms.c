/* -------------------------------------------------------------------- */
/* liste_noms.c                                                         */
/*                                                                      */
/* Gère la liste contigue contenant les noms de éléments validant ce    */
/* motif                                                                */ 
/* -------------------------------------------------------------------- */

#include "liste_noms.h"
#include "helper.h"

liste_noms_t liste_noms_creer()
{
    liste_noms_t ret;

    ret.fin = ret.debut;

    return ret;
}

size_t liste_noms_taille(liste_noms_t *liste)
{
    size_t taille = 0;

    if (liste != NULL)
    {
        taille = liste->fin - liste->debut;
    }

    return taille;
}

int liste_noms_ajouter(liste_noms_t *liste, nom_t valeur)
{
    int ret = 0;

    if (liste != NULL)
    {
        if (liste_noms_taille(liste) < LISTE_NOMS_TAILLE_MAXIMALE)
        {
            liste->fin ++;

            (*liste->fin) = valeur;

            ret = 1;
        }
    }

    return ret;
}

liste_noms_t liste_noms_par_motif(liste_semaine_t semaines, char *motif)
{
    nom_t           a_ajouter;
    int             liste_pleine = 0;
    bloc_semaine_t *cur_semaine  = NULL;
    bloc_action_t  *cur_action   = NULL;
    liste_noms_t    retour       = liste_noms_creer();

    cur_semaine = semaines.premier;

    while(cur_semaine != NULL && !liste_pleine)
    {
        cur_action = cur_semaine->actions.premier;

        while (cur_action != NULL && !liste_pleine)
        {
            if (verifie_motif(cur_action->nom, motif, 10))
            {
                a_ajouter.annee    = cur_semaine->annee;
                a_ajouter.semaine  = cur_semaine->semaine;
                a_ajouter.heure    = cur_action->heure;
                a_ajouter.intitule = cur_action->nom;
                a_ajouter.jour     = cur_action->jour;

                liste_pleine = !liste_noms_ajouter(&retour, a_ajouter);
            }
            
            cur_action = cur_action->suivant;
        }

        cur_semaine = cur_semaine->suivant;
    }

    return retour;
}

void liste_noms_afficher(liste_noms_t *liste)
{
    nom_t *cour;

    if (liste != NULL)
    {
        cour = liste->debut;

        while (cour <= liste->fin)
        {
            printf("Jour %c de la semaine %.*s de l’année %.*s à %.*sh: %.*s",
                cour->jour,
                2, cour->semaine,
                4, cour->annee,
                2, cour->heure,
                10, cour->intitule
            );
        }
    }
}