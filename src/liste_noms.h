/* -------------------------------------------------------------------- */
/* liste_noms.h                                                         */
/*                                                                      */
/* Liste contigue des événements                                        */
/*                                                                      */
/* /!\ Les valeurs contenus dans cette structure de données sont des    */
/*     pointeurs vers des valeurs définies dans les listes liste_action */
/*     et liste_semaine. La suppression de ces listes engendreront des  */
/*     erreurs sur cette liste. Ainsi, il n’y a aucune allocation       */
/*     dynamique dans les fonction définis par la suite                 */
/* -------------------------------------------------------------------- */

#ifndef LISTE_NOMS_H_INCLUDED
#define LISTE_NOMS_H_INCLUDED

#include "liste_semaine.h"

#include <stdlib.h>

/* -------------------------------------------------------------------- */
/* LISTE_NOMS_TAILLE_MAXIMALE                                           */
/*          Taille maximale que peut prendre une liste de noms          */
/* -------------------------------------------------------------------- */
#define LISTE_NOMS_TAILLE_MAXIMALE 100

/* -------------------------------------------------------------------- */
/* nom         Structure d’un élément de la liste contigue              */
/*                                                                      */
/* Champs: annee   – Chaine de caractères codant une année grégorienne  */
/*         semaine – Chaine de caractères codant une semaine sur deux   */
/*                   chiffres (01 pour javier; 12 pour décembre)        */
/*         jour    – Jour de la semaine codé sur un caractère ASCII et  */
/*                   prenant une valeur comprise entre '0' et '7'       */
/*         heure   – Heure de la semaine codé sur deux caractères ASCII */
/*                   et prenant une valeur comprise entre '00' et '24'  */
/*         nom     – Nom de l’action codé sur une chaine de 10          */
/*                   caractères                                         */
/* -------------------------------------------------------------------- */
struct nom {
    char *annee;
    char *semaine;
    char  jour;
    char *heure;
    char *intitule;
};

/* -------------------------------------------------------------------- */
/* liste_noms             Liste contigue de noms                        */
/*                                                                      */
/* Champs: fin   – Pointeur vers le dernier élément enregistré de la    */
/*                 liste                                                */
/*         debut – Liste des noms                                       */
/* -------------------------------------------------------------------- */
struct liste_noms {
    struct nom *fin;
    struct nom  debut[LISTE_NOMS_TAILLE_MAXIMALE];
};

/* -------------------------------------------------------------------- */
/* nom_t                Redéfinition de struct nom                      */
/* -------------------------------------------------------------------- */
typedef struct nom nom_t;

/* -------------------------------------------------------------------- */
/* liste_noms_t      Redéfinition de struct liste_noms                  */
/* -------------------------------------------------------------------- */
typedef struct liste_noms liste_noms_t;

/* -------------------------------------------------------------------- */
/* liste_noms_creer       Crée une liste de noms                        */
/*                                                                      */
/* En sortie: Une nouvelle liste de noms vide                           */
/* -------------------------------------------------------------------- */
liste_noms_t liste_noms_creer();

/* -------------------------------------------------------------------- */
/* liste_noms_taille    Donne la taille de la liste                     */
/*                                                                      */
/* En entrée: liste – Liste des noms                                    */
/*                                                                      */
/* En sortie: Taille de la liste actuelle ou 0 dans le cas ou la valeur */
/*            liste vaut NULL                                           */
/* -------------------------------------------------------------------- */
size_t liste_noms_taille(liste_noms_t *liste);

/* -------------------------------------------------------------------- */
/* liste_noms_ajouter  Ajoute un élément à la liste                     */
/*                                                                      */
/* En entrée: liste  – La liste des noms                                */
/*            valeur – La valeur à ajouter                              */
/*                                                                      */
/* En sortie: 1 si la valeur a bien été ajoutée ou 0 dans le cas ou la  */
/*            valeur liste vaut NULL ou la liste est pleine             */
/* -------------------------------------------------------------------- */
int liste_noms_ajouter(liste_noms_t *liste, nom_t valeur);

/* -------------------------------------------------------------------- */
/* liste_noms_par_motif                                                 */
/*    Génère une liste des actions dont l’intitulé contient le motif    */
/*                                fourni                                */
/*                                                                      */
/* En entrée: semaines – Liste des semaines où chercher les actions     */
/*            motif    – Motif à rechercher                             */
/*                                                                      */
/* En sortie: Liste des noms générés à partir des actions dont          */
/*            l’intitulé respecte le motif donné                        */
/* -------------------------------------------------------------------- */
liste_noms_t liste_noms_par_motif(liste_semaine_t semaines, char *motif);

/* -------------------------------------------------------------------- */
/* liste_noms_afficher             Affiche la liste des noms            */
/*                                                                      */
/* En entrée: liste – La liste des noms à afficher                      */
/* -------------------------------------------------------------------- */
void liste_noms_afficher(liste_noms_t *liste);

#endif
