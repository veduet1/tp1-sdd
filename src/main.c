/* -------------------------------------------------------------------- */
/* main.c                                                               */
/*                                                                      */
/* Point d’entrée du programme principal                                */
/* -------------------------------------------------------------------- */

#include "liste_semaine.h"
#include "liste_action.h"
#include "liste_noms.h"                                                     

#include <stdio.h>
#include <errno.h>
#include <sysexits.h>
#include <string.h>

#define COMMAND_UNKNOWN 0
#define COMMAND_PRINT   1
#define COMMAND_ADD     2
#define COMMAND_REMOVE  3
#define COMMAND_LSNAME  4

#define ARG_PROGNAME    0
#define ARG_FILENAME    1
#define ARG_COMMAND     2
#define ARG_COMARGS     3

/* -------------------------------------------------------------------- */
/* main.c         Point d’entrée du programme principal                 */
/*                                                                      */
/* En entrée: argc – Nombre d’arguments en entrée de la ligne de        */
/*                   commandes                                          */
/*            argv – Tableau des instructions données en ligne de       */
/*                   commandes. argv[0] correspondant à la commande     */
/*                   écrite                                             */
/*                                                                      */
/* En sortie: 0 si le programme s’est bien exécuté ou un code d’erreur  */
/*            défini dans sysexits.h choisi suivant le type d’erreur    */
/* -------------------------------------------------------------------- */
int main(int argc, char **argv)
{
    int             exit_code = EX_OK;
    int             commande  = COMMAND_UNKNOWN;
    FILE           *f         = NULL;
    liste_semaine_t semaines  = liste_semaine_creer();
    liste_noms_t    noms;

    if (argc > 0)
    {
        if (argc > 2)                                                       /* La commande spécifie normalement le fichier agenda et la commande */
        {
            if (strcmp(argv[ARG_COMMAND], "print") == 0)                    /* Vérifie si la commande est valide */
                commande = COMMAND_PRINT;
            else if (strcmp(argv[ARG_COMMAND], "add") == 0)
                commande = COMMAND_ADD;
            else if (strcmp(argv[ARG_COMMAND], "rm") == 0)
                commande = COMMAND_REMOVE;
            else if (strcmp(argv[ARG_COMMAND], "lsname") == 0)
                commande = COMMAND_LSNAME;
            else
                fprintf(stderr,
                    "La commande %s n’est pas reconnu\n",
                    argv[ARG_COMMAND]
                );

            if ((f = fopen(argv[ARG_FILENAME], "r")) != NULL)               /* Teste l’ouverture du fichier */
            {
                if (!(liste_semaine_charge_depuis_fichier(f, &semaines)))    /* Le fichier est valide */
                {
                    commande = COMMAND_UNKNOWN;
                    fprintf(stderr,
                        "Le fichier %s ne possède pas de format correct\n",
                        argv[ARG_FILENAME]
                    );
                }

                fclose(f);
            }
            else
            {
                commande = COMMAND_UNKNOWN;
                fprintf(stderr,
                    "Le fichier %s n’a pu être ouvert\n",
                    argv[ARG_FILENAME]
                );
            }

            switch (commande)
            {
            case COMMAND_PRINT:                                             /* Imprime le contenu du fichier */
                liste_semaine_afficher(semaines);
                break;
            case COMMAND_ADD:                                               /* Ajoute un événement à l’agenda */
                if (argc > 7)
                {
                    if (strlen(argv[3]) == 4 &&                             /* Vérifie si les arguments sont à la bonne taille */
                        strlen(argv[4]) == 2 &&
                        strlen(argv[5]) == 1 &&
                        strlen(argv[6]) == 2 &&
                        strlen(argv[7]) <= 10)
                    {
                        if (liste_semaine_inserer_evenement(
                            &semaines,
                            argv[3],
                            argv[4],
                            argv[5][0],
                            argv[6],
                            argv[7]
                        ))
                        {
                            if ((f = fopen(argv[ARG_FILENAME], "w")) == NULL ||
                                !liste_semaine_enregistre_vers_fichier(f, semaines))
                            {
                                fprintf(stderr, "L’enregistrement du fichier a échoué\n");
                                commande = COMMAND_UNKNOWN;
                            }
                        }
                        else
                        {
                            fprintf(stderr, "Impossible d’exécuter la commande, arguments invalides\n");
                            commande = COMMAND_UNKNOWN;
                        }
                    }
                    else
                    {
                        fprintf(stderr, "Impossible d’exécuter la commande, arguments invalides\n");
                        commande = COMMAND_UNKNOWN;
                    }
                }
                else
                {
                    fprintf(stderr, "Impossible d’exécuter la commande, nombre d’arguments insuffisant\n");
                    commande = COMMAND_UNKNOWN;
                }
                break;
            case COMMAND_REMOVE:                                            /* Supprime l’événement de l’agenda */
                if (argc > 6)
                {
                    if (strlen(argv[3]) == 4 &&                             /* Vérifie si les arguments sont à la bonne taille */
                        strlen(argv[4]) == 2 &&
                        strlen(argv[5]) == 1 &&
                        strlen(argv[6]) == 2)
                    {
                        if (liste_semaine_supprimer_action(
                            &semaines,
                            argv[3],
                            argv[4],
                            argv[5][0],
                            argv[6]
                        ))
                        {
                            if ((f = fopen(argv[ARG_FILENAME], "w")) == NULL ||
                                !liste_semaine_enregistre_vers_fichier(f, semaines))
                            {
                                fprintf(stderr, "L’enregistrement du fichier a échoué\n");
                                commande = COMMAND_UNKNOWN;
                            }
                        }
                        else
                        {
                            fprintf(stderr, "Impossible d’exécuter la commande, arguments invalides\n");
                            commande = COMMAND_UNKNOWN;
                        }
                    }
                    else
                    {
                        fprintf(stderr, "Impossible d’exécuter la commande, arguments invalides\n");
                        commande = COMMAND_UNKNOWN;
                    }
                }
                else
                {
                    fprintf(stderr, "Impossible d’exécuter la commande, nombre d’arguments insuffisant\n");
                    commande = COMMAND_UNKNOWN;
                }
                break;
            case COMMAND_LSNAME:                                            /* Liste les événements dont la description respecte le motif donné */
                if (argc > 3)
                {
                    noms = liste_noms_par_motif(semaines, argv[3]);

                    liste_noms_afficher(&noms);
                } 
                else
                {
                    fprintf(stderr, "Impossible d’exécuter la commande, nombre d’arguments insuffisant\n");
                    commande = COMMAND_UNKNOWN;
                }
                break;
            }
        }
        
        if (commande == COMMAND_UNKNOWN)
        {
            exit_code = EX_NOINPUT;
            fprintf(
                stderr,
                "Arguments: %s <fichier agenda> <commande> <données>\n",
                argv[ARG_PROGNAME]
            );
            fprintf(stderr, "Avec <commande>:\n");
            fprintf(stderr, "\tprint\tAffiche le contenu de l’agenda\n");
            fprintf(stderr, "\tadd\tAjouter une tâche à l’agenda avec le format `AAAA SS J HH Intitule`\n");
            fprintf(stderr, "\trm\tSupprimer une tâche de l’agenda à la position donné par `AAAA SS J HH`\n");
            fprintf(stderr, "\tlsname\tListe les tâches de l’agenda à condition que leur nom contient un motif donné par <données>\n");
        }
    }

    liste_semaine_liberer(&semaines);

    return exit_code;
}
