/* -------------------------------------------------------------------- */
/* liste_action.c                                                       */
/*                                                                      */
/* Gère la liste chainée codant les action dans la semaine              */
/* -------------------------------------------------------------------- */

#include "liste_action.h"
#include "helper.h"

#include <stdlib.h>
#include <string.h>

/* -------------------------------------------------------------------- */
/*                                                                      */
/* En-tête des procédures privées                                       */
/*                                                                      */
/* -------------------------------------------------------------------- */

/* -------------------------------------------------------------------- */
/* resultat_recherche_bloc_action                                       */
/*     Structure d’un résultat de recherche d’une action de la liste    */
/*                                                                      */
/* Champs: element   – Pointeur vers l’élément recherché si trouvé ou   */
/*                     NULL sinon                                       */
/*         precedent - Pointeur vers l’élément précédent l’élément      */
/*                     recherché si ce dernier est trouvé et ne se      */
/*                     trouve pas en première position ou NULL sinon    */
/* -------------------------------------------------------------------- */
struct resultat_recherche_bloc_action {
    struct bloc_action* element;
    struct bloc_action* precedent;
};

/* -------------------------------------------------------------------- */
/* liste_action_recherche_intermediaire                                 */
/*                    Récupère l’adresse de l’action                    */
/*                                                                      */
/* En entrée: liste - Liste des actions où rechercher l’action          */
/*            jour  - Jour de l’action recherchée                       */
/*            heure - Heure de l’action recherchée                      */
/*                                                                      */
/* En sortie: Un objet contenant les adresse de l’action recherchée     */
/*            dans le champ element et celle de l’action la précédente  */
/*            dans la liste (eg celle dont le champ suivant pointe sur  */
/*            la même case mémoire que element) dans precedent          */
/* -------------------------------------------------------------------- */
static                                                                      /* Cette fonction est uniquement utilisé dans ce fichier et on souhaite qu’il ne soit pas utilisé en dehors */
struct resultat_recherche_bloc_action
liste_action_recherche_intermediaire(
    liste_action_t liste,
    char           jour,
    char          *heure)
{
    struct resultat_recherche_bloc_action ret   = { NULL, NULL };
    bloc_action_t                        *cour1 = liste.premier;
    bloc_action_t                        *cour2 = NULL;

    while (ret.element == NULL && cour1 != NULL) {                          /* On parcours la liste tant qu’on n’a rien trouvé */
        if (cour1->jour == jour && cmp_tableau(cour1->heure, heure, 2))
        {                                                                   /* On a trouvé une occurence, on enregistre l’adresse de l’élément courant ainsi que celui qui le précède dans la liste */
            ret.element = cour1;
            ret.precedent = cour2;
        }

        cour2 = cour1;
        cour1 = cour1->suivant;
    }

    return ret;
}

/* -------------------------------------------------------------------- */
/*                                                                      */
/* Implémentation des fonctions                                         */
/*                                                                      */
/* -------------------------------------------------------------------- */

liste_action_t liste_action_creer(void)
{
    return (liste_action_t){ NULL };
}

void liste_action_liberer(liste_action_t *liste) {
    bloc_action_t *cour = NULL;
    bloc_action_t *suiv = NULL;

    if (liste != NULL)
    {
        cour = liste->premier;
        liste->premier = NULL;

        while (cour != NULL)
        {
            suiv = cour->suivant;

            free(cour);

            cour = suiv;
        }
    }
}

int liste_action_inserer(liste_action_t *liste,
                         char            jour,
                         char           *heure,
                         char           *nom)
{
    bloc_action_t *a_ajouter = NULL;
    int            ok        = 1;

    if (liste != NULL && heure != NULL && nom != NULL)
    {
        a_ajouter = malloc(sizeof(bloc_action_t));

        if (a_ajouter != NULL)
        {
            if (!compris_entre(jour, '1', '7') ||
                !cpy_liste_chiffre(
                    a_ajouter->heure, heure, "00", "23", 2))
            {
                ok = 0;
                free(a_ajouter);
            }
            else
            {
                a_ajouter->jour = jour;
                cpy_desc(a_ajouter->nom, nom, 10);
                a_ajouter->suivant = liste->premier;
                liste->premier = a_ajouter;
            }
        }
    }

    return ok;
}

bloc_action_t *liste_action_supprimer(liste_action_t *liste)
{
    bloc_action_t *case_retire = NULL;

    if (liste != NULL)
    {
        case_retire = liste->premier;

        if (case_retire != NULL)
        {
            liste->premier = case_retire->suivant;
            case_retire->suivant = NULL;
        }
    }

    return case_retire;
}

int liste_action_supprimer_par_donnees(liste_action_t *liste,
                                       char            jour,
                                       char           *heure)
{
    struct resultat_recherche_bloc_action act;
    int                                   ok  = 0;

    act = liste_action_recherche_intermediaire(*liste, jour, heure);

    if (act.element != NULL)
    {
        if (act.precedent != NULL)                                          /* Modification de la liste afin d’isoler l’élément à supprimer */
        {
            act.precedent->suivant = act.element->suivant;
        }
        else
        {
            liste->premier = act.element->suivant;
        }

        free(act.element);                                                  /* Libération de la mémoire allouée pour l’action à supprimer */

        ok = 1;
    }

    return ok;
}

bloc_action_t *liste_action_recherche(liste_action_t liste,
                                      char           jour,
                                      char          *heure)
{
    return liste_action_recherche_intermediaire(liste, jour, heure).element;
}