tp1 : liste_action.dbg.o liste_semaine.dbg.o liste_noms.dbg.o main.dbg.o helper.dbg.o
	gcc -o tp1.dbg liste_action.dbg.o liste_semaine.dbg.o liste_noms.dbg.o main.dbg.o helper.dbg.o -g

liste_action.o : src/liste_action.c
	gcc -Wall -Wextra -Wvla -Werror -ansi -c src/liste_action.c -o liste_action.o

liste_semaine.o : src/liste_semaine.c
	gcc -Wall -Wextra -Wvla -Werror -ansi -c src/liste_semaine.c -o liste_semaine.o

liste_noms.o : src/liste_noms.c
	gcc -Wall -Wextra -Wvla -Werror -ansi -c src/liste_noms.c -o liste_noms.o

main.o : src/main.c
	gcc -Wall -Wextra -Wvla -Werror -ansi -c src/main.c -o main.o

helper.o : src/helper.c
	gcc -Wall -Wextra -Wvla -Werror -ansi -c src/helper.c -o helper.o

liste_action.dbg.o : src/liste_action.c
	gcc -Wall -Wextra -Wvla -Wdeclaration-after-statement -g -c src/liste_action.c -o liste_action.dbg.o

liste_semaine.dbg.o : src/liste_semaine.c
	gcc -Wall -Wextra -Wvla -Wdeclaration-after-statement -g -c src/liste_semaine.c -o liste_semaine.dbg.o

liste_noms.dbg.o : src/liste_noms.c
	gcc -Wall -Wextra -Wvla -Wdeclaration-after-statement -g -c src/liste_noms.c -o liste_noms.dbg.o

main.dbg.o : src/main.c
	gcc -Wall -Wextra -Wvla -Wdeclaration-after-statement -g -c src/main.c -o main.dbg.o

helper.dbg.o : src/helper.c
	gcc -Wall -Wextra -Wvla -Wdeclaration-after-statement -g -c src/helper.c -o helper.dbg.o

release : liste_action.o liste_semaine.o liste_noms.o main.o helper.o
	gcc -o tp1 liste_action.o liste_semaine.o liste_noms.o main.o helper.o

clean :
	rm -f *.o tp1 tp1.dbg